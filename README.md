This is the project for the backend of YEC tuning website.
The framework is Django and using Python 3.7

1. Build a website for a car performance tuning shop. Refer to this website https://www.br-performance.be/en-be/
2. The backend must use Django
3. The website needs to be English and Chinese
4. There are a few major topics
    
    4.1 User can choose car brand, model and chassis and the website will display car performance improvement after tuning.Our car shop donot have data yet. Have to crawl the page like this. https://www.br-performance.be/en-be/chiptuning/1-cars/11-audi/209-a2/210-8z-1999-2005/212-1-4-tdi/  Then, when we have the data, we can manually update the database and upload a few pictures related to that specific car.

    4.2 eCommerce to display and sell physical car parts/products based on car brand, model and chassis.

    4.3 eCommerce to sell virtual products, thousands of downloadable softwares. User can search based on fileName, which can be either Chinese or English. The files are saved at a ftp server. The users pay for yearly subscription and can download as many as they want. 

    4.4 A user tuning submission system that user input car brand, model, chassis and VIN to request a tuning service. A sample page will be provided later.

    4.5 A blog to publish the latest news.

    4.6 A picture display system so that admin can upload pictures and rolling display them at the front page.  

5. Admin system
    
    5.1 Admin can upload pictures to display on the home page and other pages.

    5.2 Admin will upload thousands of downloadable softwares to a ftp server and sell them at this website. 

    5.3 Admin can update the car tuning performance data. Django's admin system should be good enough.

    5.4 Admin can collect money from paypal and sell parts.

    5.5 Admin can write blog to publish

    5.6 Admin can decide how much to sell everything

6. Technical requirements
    
    6.1 Use git to submit codes.

    6.2 Use Django for backend.

    6.3 Use mySql for database.

    6.4 Deploy as a Django project. The work will be verified by Django $ python manage.py runserver at localhost. 

    6.5 Frontend use React + Material UI(if applicable)

    6.6 Use python 3.7

    6.7 Production deploy will use wsgi at Heroku
